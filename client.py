import socket
import json
from constants import HOST, PORT, BUFFER, ADDRESS_FAMILY, SOCKET_TYPE, ENCODING_FORMAT

class Client:
    def __init__(self):
        self.host = HOST
        self.port = PORT
        self.buffer = BUFFER
        self.address_family = ADDRESS_FAMILY
        self.socket_type = SOCKET_TYPE
        self.is_running = True

    def serialise_and_encode(self, data):
        return json.dumps(data).encode(ENCODING_FORMAT)

    def deserialise(self, data):
        return json.loads(data)

    def sign_up(self, password="abc", repeated_password="123"):
        username = input("Your username: ")
        while password != repeated_password:
            password = input("Your password: ")
            repeated_password = input("Repeat password: ")

        sign_up_dict = {
            "username": username,
            "password": password
        }
        return sign_up_dict

    def sign_in(self):
        username = input("Your username: ")
        password = input("Your password: ")

        sign_in_dict = {
            "username": username,
            "password": password
        }
        return sign_in_dict

    def send_message(self):
        recipient = input("Recipient's username: ")
        message = input("Message: ")
        message_dict = {
            "recipient": recipient,
            "message": message
        }
        return message_dict

    def compose_request(self, user_command):
        request_dict = {
            "command": user_command,
            "data": {}
        }
        if user_command == "sign up":
            new_user_data = self.sign_up()
            if new_user_data is not None:
                request_dict['data'] = new_user_data
        elif user_command == "sign in":
            sign_in_data = self.sign_in()
            if sign_in_data is not None:
                request_dict['data'] = sign_in_data
        elif user_command == "send message":
            message_content = self.send_message()
            request_dict['data'] = message_content

        return request_dict

    def parse_response(self, response):
        parsed_response = ""
        parsed_single_message = ""

        if isinstance(response, list):
            for i in range(len(response)):
                single_message = [f"{key}: {value}" for key, value in response[i].items()]
                parsed_single_message += '\n'.join(single_message)
                message_no = i + 1
                message_no_text_line = f"Message {message_no}: \n"
                parsed_response += f"{message_no_text_line}{parsed_single_message}\n\n"
                parsed_single_message = ""

            return parsed_response.rstrip('\n')
        else:
            parsed_response = [f"{key}: {value}" for key, value in response.items()]
            return '\n'.join(parsed_response)

    def stop_requested(self, server_response_json):
        if not isinstance(server_response_json, list):
            for key, value in server_response_json.items():
                if key == "response" and value == "Connection terminated":
                    return True

        return False

    def run(self):
        with socket.socket(ADDRESS_FAMILY, SOCKET_TYPE) as client_socket:
            client_socket.connect((HOST, PORT))
            server_response = client_socket.recv(BUFFER)
            server_response_json = self.deserialise(server_response)
            print(self.parse_response(server_response_json))

            while self.is_running:
                print("> ", end="")
                user_command = input()
                client_request = self.compose_request(user_command)
                client_request_json = self.serialise_and_encode(client_request)
                client_socket.sendall(client_request_json)

                server_response = client_socket.recv(BUFFER)
                server_response_json = self.deserialise(server_response)
                print(self.parse_response(server_response_json))
                if self.stop_requested(server_response_json):
                    self.is_running = False
                    client_socket.close()


if __name__ == "__main__":
    client = Client()
    client.run()