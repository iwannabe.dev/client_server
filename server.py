import socket
import json
from datetime import datetime
from constants import HOST, PORT, BUFFER, ADDRESS_FAMILY, SOCKET_TYPE, ENCODING_FORMAT
from user import UserAccountManager


class Server:
    def __init__(self):
        self.host = HOST
        self.port = PORT
        self.buffer = BUFFER
        self.address_family = ADDRESS_FAMILY
        self.socket_type = SOCKET_TYPE
        self.version = "1.1.0"
        self.startup_time = datetime.now()
        self.is_running = True
        self.request_validator = RequestValidator()
        self.response_handler = ResponseHandler(self)

    def serialise_and_encode(self, response):
        return json.dumps(response).encode(ENCODING_FORMAT)

    def deserialise(self, data):
        return json.loads(data)

    def greet_client(self, client_ip, client_port):
        greetings_message = f"Connection established from [IP:PORT]: " \
                            f"{client_ip}:{client_port}. \n" \
                            f"Type in 'help' to see all available commands: "
        return {"response": greetings_message}

    def handle_client_request(self, client_request):
        client_request_json = self.deserialise(client_request)

        # validate the request
        is_request_valid = self.request_validator.validate_request(client_request_json)
        if not is_request_valid:
            return {"response": "Request validation failed."}

        # handle the request
        server_response = self.response_handler.handle_request(client_request_json)

        return server_response

    def stop_server(self):
        self.is_running = False

    def run(self):
        with socket.socket(self.address_family,self.socket_type) as server_socket:
            server_socket.bind((HOST, PORT))
            server_socket.listen()

            client_socket, address = server_socket.accept()
            client_ip = address[0]
            client_port = address[1]

            greetings_json = self.serialise_and_encode(self.greet_client(client_ip, client_port))
            client_socket.sendall(greetings_json)

            with client_socket:
                while self.is_running:
                    client_request = client_socket.recv(self.buffer)
                    server_response = self.handle_client_request(client_request)
                    server_response_serialized = self.serialise_and_encode(server_response)
                    client_socket.sendall(server_response_serialized)


class RequestValidator:
    def __init__(self):
        self.allowed_commands = [
            "help",
            "uptime",
            "info",
            "sign up",
            "sign in",
            "sign out",
            "delete account",
            "send message",
            "inbox",
            "stop"
        ]

    def validate_request(self, client_request_json):
        request_command = client_request_json['command']
        user_data = client_request_json['data']

        if request_command in self.allowed_commands:
            if request_command == "sign up" or request_command == "sign in":
                return self.validate_user_login_data(user_data)
            elif request_command == "send message":
                return self.validate_send_message_data(user_data)
            return True
        else:
            return False

    def validate_user_login_data(self, user_data):
        required_fields = ["username", "password"]
        for field in required_fields:
            if field not in user_data:
                return False

        return True

    def validate_send_message_data(self, user_data):
        required_fields = ["recipient", "message"]
        for field in required_fields:
            if field not in user_data:
                return False
        if len(user_data['message']) > 250:
            return False

        return True


class ResponseHandler:
    def __init__(self, server):
        self.server = server
        self.account_manager = UserAccountManager()

    def handle_request(self, client_request_json):
        command = client_request_json['command']
        user_data = client_request_json['data']

        if command == "info":
            return self.handle_info_request()
        elif command == "sign up":
            return self.handle_sign_up_request(user_data)
        elif command == "sign in":
            return self.handle_sign_in_request(user_data)
        elif command == "sign out":
            return self.handle_sign_out_request()
        elif command == "delete account":
            return self.handle_delete_account_request()
        elif command == "send message":
            return self.handle_send_message_request(user_data)
        elif command == "inbox":
            return self.handle_check_inbox_request()
        elif command == "stop":
            return self.handle_stop_server_request()
        elif command == "help":
            return  self.handle_help_request()
        elif command == "uptime":
            return self.handle_uptime_request()
        else:
            return {"response": "Unknown command"}

    def handle_info_request(self):
        formatted_startup_time = str(self.server.startup_time).split(".")[0]
        info_dict = {
            "version": self.server.version,
            "created": formatted_startup_time
        }

        return info_dict

    def handle_sign_up_request(self, user_data):
        if not self.account_manager.username_exists(user_data['username']):
            if self.account_manager.is_logged_in() and self.account_manager.is_admin(self.account_manager.logged_in_user):
                self.account_manager.make_admin_account(user_data)
                return {"response": "New user account created. You can 'sign in' now."}

            if self.account_manager.is_logged_in():
                return {"response": "You are already signed in. You can't make another account."}
            else:
                self.account_manager.make_user_account(user_data)
                return {"response": "New user account created. You can 'sign in' now."}
        else:
            return {"response": "This username is already taken. Try again with a different username."}

    def handle_sign_in_request(self, user_data):
        if self.account_manager.is_logged_in():
            return {"response": "You are already signed in."}

        if self.account_manager.password_matched(user_data):
            self.account_manager.logged_in_user = user_data['username']
            return {"response": "You've been successfully signed in."}

        return {"response": "Wrong username and/or password. Try again."}

    def handle_sign_out_request(self):
        if self.account_manager.is_logged_in():
            self.account_manager.logged_in_user = None
            return {"response": "User has been signed out."}
        else:
            return {"response": "You are not currently signed in."}

    def handle_delete_account_request(self):
        if self.account_manager.is_logged_in():
            self.account_manager.delete_user()
            self.handle_sign_out_request()
            return {"response": "User account has been deleted and signed out."}
        else:
            return {"response": "You are not currently signed in."}

    def handle_send_message_request(self, message_data):
        if not self.account_manager.is_logged_in():
            return {"response": "You are not currently signed in."}

        sender_username = self.account_manager.logged_in_user
        recipient_username = message_data['recipient']

        if self.account_manager.username_exists(recipient_username):
            if self.account_manager.inbox_full(recipient_username) and \
                    not (self.account_manager.is_admin(sender_username) or
                    self.account_manager.is_admin(recipient_username)):
                return {"response": "Sending failed. Recipient's inbox is full."}
            else:
                self.account_manager.send_message(message_data)
                return {"response": "Message has been sent successfully."}
        else:
            return {"response": "Sending failed. Recipient doesn't exist."}

    def handle_check_inbox_request(self):
        if self.account_manager.is_logged_in():
            return self.account_manager.check_inbox()
        else:
            return {"response": "You are not currently signed in."}

    def handle_stop_server_request(self):
        if self.account_manager.is_logged_in():
            # self.account_manager.save_db()
            self.server.stop_server()
            return {"response": "Connection terminated."}
        else:
            return {"response": "You are not authorized to stop the server."}

    def handle_help_request(self):
        commands_dict = {
            "'help'": "shows all available commands that server understands.",
            "'uptime'": "shows actual uptime of the server",
            "'info'": "shows actual version of the server app and its creation date",
            "'sign in'": "creates new user account",
            "'sign up'": "creates new user account",
            "'stop'": "stops both, server and client"
        }

        extra_commands_dict = {
            "'sign out'": "signs out current user",
            "'delete account'": "deletes current user account",
            "'send message'": "sends a message (max 250 characters) to another user",
            "'inbox'": "checks for messages in inbox"
        }

        if not self.account_manager.is_logged_in():
            return commands_dict
        else:
            commands_dict.update(extra_commands_dict)
            return commands_dict

    def handle_uptime_request(self):
        current_time = datetime.now()
        server_uptime = current_time - self.server.startup_time

        return {"uptime": str(server_uptime)}


if __name__ == "__main__":
    server = Server()
    print("Server is running...")
    server.run()