from db_manager import DBManager
from psycopg2 import sql
from decouple import config


def create_db(db_manager, db_name):
    query = sql.SQL("CREATE DATABASE {db_name};").format(db_name=sql.Identifier(db_name))
    db_manager.execute_query(query)

def create_tables(db_manager):
    server_user_table_query = """
            CREATE TABLE IF NOT EXISTS server_user (
            user_id SERIAL PRIMARY KEY,
            username VARCHAR(50) NOT NULL UNIQUE,
            password VARCHAR(50) NOT NULL,
            role VARCHAR(5) NOT NULL
            );
    """

    message_table_query = """
        CREATE TABLE IF NOT EXISTS message (
            user_id INTEGER NOT NULL,
            message_id SERIAL,
            received TIMESTAMPTZ NOT NULL DEFAULT NOW(),
            sender VARCHAR(50) NOT NULL,
            message VARCHAR(250) NOT NULL,
            PRIMARY KEY (user_id, message_id)
        );
    """

    db_manager.execute_query(server_user_table_query)
    db_manager.execute_query(message_table_query)

def add_default_admin(db_manager):
    add_admin_query = "INSERT INTO server_user (username, password, role) VALUES ('admin', '1234', 'admin');"
    db_manager.execute_query(add_admin_query)


if __name__ == "__main__":
    db_manager = DBManager(db_name='postgres')
    new_db_name = '1234_test'
    if not db_manager.database_exists(new_db_name):
        create_db(db_manager, new_db_name)
        del db_manager
        new_db_manager = DBManager(db_name=new_db_name)
        create_tables(new_db_manager)
        add_default_admin(new_db_manager)
