import socket

HOST = "127.0.0.1"
PORT = 65432
BUFFER = 1024
ADDRESS_FAMILY = socket.AF_INET
SOCKET_TYPE = socket.SOCK_STREAM

ENCODING_FORMAT = "utf-8"
