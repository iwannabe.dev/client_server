import unittest
from server import Server, RequestValidator, ResponseHandler


class TestResponseHandler(unittest.TestCase):
    def setUp(self) -> None:
        self.response_handler = ResponseHandler(Server())

    def test_response_to_valid_sign_in_request(self):
        # given
        valid_sign_in_request = {
            'command': 'sign in', 'data': {'username': 'admin', 'password': 'qwerty'}
        }

        # when
        server_response_to_valid_sign_in = self.response_handler.handle_request(valid_sign_in_request)

        # then
        expected_response_to_valid_sign_in = {'response': "You've been successfully signed in."}
        self.assertEqual(expected_response_to_valid_sign_in, server_response_to_valid_sign_in)

    def test_response_to_invalid_sign_in_request(self):
        # given
        invalid_username_sign_in_request = {
            'command': 'sign in', 'data': {'username': 'Bdmin', 'password': 'qwerty'}
        }

        # when
        server_response_to_invalid_username = self.response_handler.handle_request(invalid_username_sign_in_request)

        # then
        expected_response_to_invalid_username = {'response': 'Wrong username and/or password. Try again.'}
        self.assertEqual(expected_response_to_invalid_username, server_response_to_invalid_username)

    def test_response_to_valid_send_message_request_when_signed_in(self):
        # given
        valid_send_message_request = {
            'command': 'send message', 'data': {'recipient': 'admin', 'message': 'Hello admin.'}
        }
        valid_sign_in_request = {
            'command': 'sign in', 'data': {'username': 'admin', 'password': 'qwerty'}
        }

        # when
        self.response_handler.handle_request(valid_sign_in_request)  # signing in user to allow to send messages
        server_response_to_valid_send_message = self.response_handler.handle_request(valid_send_message_request)

        # then
        expected_response_to_successfull_send_message = {'response': 'Message has been sent successfully.'}
        self.assertEqual(expected_response_to_successfull_send_message, server_response_to_valid_send_message)

    def test_response_to_invalid_send_message_request_when_signed_in(self):
        # given
        invalid_recipient_send_message_request = {
            'command': 'send message', 'data': {'recipient': 'user666', 'message': 'Hello user666.'}
        }
        valid_sign_in_request = {
            'command': 'sign in', 'data': {'username': 'admin', 'password': 'qwerty'}
        }

        # when
        self.response_handler.handle_request(valid_sign_in_request)  # signing in user to allow to send messages
        server_response_to_invalid_recipient = self.response_handler.handle_request(
                                                    invalid_recipient_send_message_request)

        # then
        expected_response_to_invalid_recipient = {'response': "Sending failed. Recipient doesn't exist."}
        self.assertEqual(expected_response_to_invalid_recipient, server_response_to_invalid_recipient)

    def test_response_to_valid_send_message_request_when_not_signed_in(self):
        # given
        valid_send_message_request = {
            'command': 'send message', 'data': {'recipient': 'admin', 'message': 'Hello admin.'}
        }

        # when
        server_response_to_valid_send_message = self.response_handler.handle_request(valid_send_message_request)

        # then
        expected_response_to_send_message_when_not_signed_in = {'response': 'You are not currently signed in.'}
        self.assertEqual(expected_response_to_send_message_when_not_signed_in, server_response_to_valid_send_message)


class TestRequestValidator(unittest.TestCase):
    def setUp(self) -> None:
        self.request_validator = RequestValidator()

    def test_validation_of_sign_up_request(self):
        # given
        sign_up_request = {'command': 'sign up', 'data': {
            'username': 'user1', 'password': '123'}}

        # when
        can_sign_up = self.request_validator.validate_request(sign_up_request)

        # then
        self.assertTrue(can_sign_up)

    def test_validation_of_help_request(self):
        # given
        help_request = {'command': 'help', 'data': {}}

        # when
        can_get_help = self.request_validator.validate_request(help_request)

        # then
        self.assertTrue(can_get_help)

    def test_validation_of_info_request(self):
        # given
        info_request = {'command': 'info', 'data': {}}

        # when
        can_get_info = self.request_validator.validate_request(info_request)

        # then
        self.assertTrue(can_get_info)

    def test_validation_of_send_message_request(self):
        # given
        send_message_request = {'command': 'send message', 'data': {
            'recipient': 'admin', 'message': 'Hello admin.'}}

        # when
        can_send_message = self.request_validator.validate_request(send_message_request)

        # then
        self.assertTrue(can_send_message)

if __name__ == "__main__":
    unittest.main()
