import unittest
from user import UserAccountManager


class TestUserAccountManager(unittest.TestCase):
    def setUp(self) -> None:
        self.account_manager = UserAccountManager()
    def test_is_admin(self):
        """Test that given username has 'admin' privileges."""

        # given
        username = "admin"

        # when
        result = self.account_manager.is_admin(username)

        # then
        self.assertTrue(result)

    def test_is_not_logged_in(self):
        """Test that no one is logged in (that's default state of the app)."""

        # when
        result = self.account_manager.is_logged_in()

        # then
        self.assertFalse(result)

    def test_inbox_not_full(self):
        """Test that the inbox of a given username is not full."""

        # given
        username = "admin"

        # when
        result = self.account_manager.inbox_full(username)

        # then
        self.assertFalse(result)
