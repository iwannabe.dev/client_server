from db_manager import DBManager
from psycopg2 import sql


class UserAccountManager:
    def __init__(self):
        self.logged_in_user = None
        self.db_manager = DBManager()

    def _get_user_id(self, username):
        query = sql.SQL("SELECT user_id FROM server_user WHERE username = {}").format(sql.Literal(username))
        result = self.db_manager.execute_query(query).fetchall()

        if result:
            user_id = result[0][0]
            return user_id

    def username_exists(self, username):
        query = sql.SQL("SELECT username FROM server_user WHERE username = {}").format(sql.Literal(username))
        result = self.db_manager.execute_query(query).fetchall()

        if not result:
            return False

        return True

    def is_admin(self, username):
        query = sql.SQL("SELECT role FROM server_user WHERE username = {}").format(sql.Literal(username))
        result = self.db_manager.execute_query(query).fetchall()

        if result:
            role = result[0][0]

        if not result or role != "admin":
            return False

        return True

    def is_logged_in(self):
        if not self.logged_in_user:
            return False

        return True

    def make_user_account(self, user_data, role="user"):
        username = user_data['username']
        password = user_data['password']
        new_user_data = [username, password, role]

        query = sql.SQL("INSERT INTO server_user (username, password, role) VALUES (%s, %s, %s)")
        self.db_manager.execute_query(query, new_user_data)

    def make_admin_account(self, user_data):
        return self.make_user_account(user_data, role="admin")

    def password_matched(self, user_data):
        username = user_data['username']
        password = user_data['password']

        query = sql.SQL("SELECT password FROM server_user WHERE username = {}").format(sql.Literal(username))
        result = self.db_manager.execute_query(query).fetchall()

        if result:
            retrieved_password = result[0][0]

        if not result or retrieved_password != password:
            return False

        return True

    def delete_user(self):
        user_id = self._get_user_id(self.logged_in_user)

        delete_user_query = sql.SQL("""
            DELETE FROM server_user 
            WHERE user_id = {}""").format(sql.Literal(user_id))
        self.db_manager.execute_query(delete_user_query)

        delete_messages_query = sql.SQL("""
            DELETE FROM message 
            WHERE user_id = {}""").format(sql.Literal(user_id))
        self.db_manager.execute_query(delete_messages_query)

    def send_message(self, message_data):
        recipient = message_data['recipient']
        recipient_id = self._get_user_id(recipient)
        sender = self.logged_in_user
        message = message_data['message']
        query_parameters = [recipient_id, sender, message]

        query = sql.SQL("INSERT INTO message (user_id, sender, message) VALUES (%s, %s, %s)")
        self.db_manager.execute_query(query, query_parameters)

    def check_inbox(self):
        messages = []
        user_id = self._get_user_id(self.logged_in_user)

        query = sql.SQL("SELECT sender, message FROM message WHERE user_id = {}").format(sql.Literal(user_id))
        result = self.db_manager.execute_query(query)

        messages_list = result.fetchall()

        # Converting list of tuples (containing messages and metadata) to list of dictionaries
        dict_keys = ("sender", "message")
        for single_message_tuple in messages_list:
            single_message_dict = dict(zip(dict_keys, single_message_tuple))
            messages.append(single_message_dict)

        if not messages:
            return {"inbox": "no messages"}

        return messages

    def inbox_full(self, recipient):
        recipient_id = self._get_user_id(recipient)
        query = sql.SQL("SELECT COUNT(user_id) FROM message WHERE user_id = {}").format(sql.Literal(recipient_id))
        result = self.db_manager.execute_query(query)
        message_count = result.fetchone()[0]

        if message_count > 5:
            return True

        return False