import psycopg2
from psycopg2 import OperationalError, ProgrammingError
from decouple import config


class DBManager():
    def __init__(self, db_name=None):
        self.db_user = config('USERNAME')
        self.db_password = config('PASSWORD')
        self.db_host = config('HOST')
        self.db_port = config('PORT')
        self.db_name = db_name or config('DB_NAME')
        self.connection = self.create_db_connection()

    def create_db_connection(self):
        connection = None
        try:
            connection = psycopg2.connect(
                user=self.db_user,
                password=self.db_password,
                host=self.db_host,
                port=self.db_port,
                database=self.db_name
            )
            connection.autocommit = True
            print(f"Connection to '{self.db_name}' DB successful")
        except OperationalError as e:
            print(f"The error '{e}' occurred")

        return connection

    def execute_query(self, query, parameters=None):
        cursor = self.connection.cursor()
        try:
            cursor.execute(query, parameters)
            print(f"Query '{query}' executed successfully")
            return cursor
        except OperationalError as e:
            print(f"The error '{e}' occurred")
        except psycopg2.errors.UniqueViolation as pg_err:
            print(f"The error '{pg_err}' occurred")

    def database_exists(self, db_name):
        database_list_query = "SELECT datname FROM pg_database;"

        try:
            database_list = self.execute_query(database_list_query)

            for tup in database_list:
                if tup[0] == db_name:
                    return True

            return False

        except OperationalError as e:
            print(f"The error '{e}' occurred")